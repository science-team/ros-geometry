Source: ros-geometry
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Jochen Sprickerhof <jspricke@debian.org>,
 Leopold Palomo-Avellaneda <leo@alaxarxa.net>,
 Timo Röhling <roehling@debian.org>,
Build-Depends:
 catkin,
 debhelper-compat (= 13),
 dh-ros,
 dh-sequence-python3,
 libactionlib-dev,
 libactionlib-msgs-dev,
 libangles-dev,
 libboost-dev,
 libboost-filesystem-dev,
 libboost-regex-dev,
 libboost-thread-dev,
 libbullet-dev,
 libconsole-bridge-dev,
 libeigen3-dev,
 libgeometry-msgs-dev,
 libgtest-dev,
 libmessage-filters-dev,
 liborocos-kdl-dev,
 libpython3-dev,
 libros-rosgraph-msgs-dev,
 librosconsole-dev,
 libroscpp-core-dev,
 libroscpp-dev,
 libroscpp-msg-dev,
 librostest-dev,
 libsensor-msgs-dev,
 libstd-msgs-dev,
 libtf2-dev,
 libtf2-msgs-dev,
 libtf2-ros-dev,
 libxmlrpcpp-dev,
 python3-all,
 python3-numpy <!nocheck>,
 python3-pykdl <!nocheck>,
 python3-rospy,
 python3-rostest <!nocheck>,
 python3-sensor-msgs <!nocheck>,
 python3-setuptools,
 python3-tf2,
 python3-tf2-ros <!nocheck>,
 ros-actionlib-msgs,
 ros-geometry-msgs,
 ros-message-generation,
 ros-rosgraph-msgs,
 ros-sensor-msgs,
 ros-std-msgs,
 ros-tf2-msgs,
Standards-Version: 4.7.0
Section: libs
Rules-Requires-Root: no
Homepage: https://wiki.ros.org/geometry
Vcs-Browser: https://salsa.debian.org/science-team/ros-geometry
Vcs-Git: https://salsa.debian.org/science-team/ros-geometry.git

Package: libtf1d
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Robot OS tf transform library to keep track of multiple coordinate frames
 tf is a package that lets the user keep track of multiple coordinate frames
 over time. tf maintains the relationship between coordinate frames in a tree
 structure buffered in time, and lets the user transform points, vectors, etc
 between any two coordinate frames at any desired point in time.
 .
 This package contains the library itself.

Package: cl-tf
Section: lisp
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: Robot OS tf transform library -- LISP interface
 tf is a package that lets the user keep track of multiple coordinate frames
 over time. tf maintains the relationship between coordinate frames in a tree
 structure buffered in time, and lets the user transform points, vectors, etc
 between any two coordinate frames at any desired point in time.
 .
 This package contains the LISP bindings (messages and services) and
 is part of Robot OS (ROS).

Package: libtf-conversions-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libeigen3-dev,
 libgeometry-msgs-dev,
 liborocos-kdl-dev,
 libtf-conversions0d ( = ${binary:Version}),
 libtf-dev,
 ${misc:Depends},
Description: Robot OS conversion library between Eigen, KDL and tf - development files
 This package contains a set of conversion functions to convert
 common tf datatypes (point, vector, pose, etc) into semantically
 identical datatypes used by other libraries. The conversion functions
 make it easier for users of the transform library (tf) to work with
 the datatype of their choice. Currently this package has support for
 the Kinematics and Dynamics Library (KDL) and the Eigen matrix library.
 .
 This package contains the development files of tf-conversions library
 and is part of Robot OS (ROS).

Package: libtf-conversions0d
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Multi-Arch: same
Description: Robot OS conversion library between tf and Eigen and KDL
 This package contains a set of conversion functions to convert
 common tf datatypes (point, vector, pose, etc) into semantically
 identical datatypes used by other libraries. The conversion functions
 make it easier for users of the transform library (tf) to work with
 the datatype of their choice. Currently this package has support for
 the Kinematics and Dynamics Library (KDL) and the Eigen matrix library.
 .
 This package contains the library itself and is part of Robot OS (ROS).

Package: libtf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends:
 libgeometry-msgs-dev,
 libmessage-filters-dev,
 librosconsole-dev,
 libroscpp-dev,
 libsensor-msgs-dev,
 libstd-msgs-dev,
 libtf1d ( = ${binary:Version}),
 libtf2-ros-dev,
 ros-message-runtime,
 ${misc:Depends},
Description: Robot OS tf transform library  - development files
 tf is a package that lets the user keep track of multiple coordinate frames
 over time. tf maintains the relationship between coordinate frames in a tree
 structure buffered in time, and lets the user transform points, vectors, etc
 between any two coordinate frames at any desired point in time.
 .
 This package contains the development files of tf library.

Package: python3-tf
Section: python
Architecture: any
Depends:
 python3-genpy,
 python3-geometry-msgs,
 python3-numpy,
 python3-sensor-msgs,
 python3-std-msgs,
 python3-tf2-ros,
 ${misc:Depends},
 ${python3:Depends},
Description: Robot OS tf transform library - Python 3
 tf is a package that lets the user keep track of multiple coordinate frames
 over time. tf maintains the relationship between coordinate frames in a tree
 structure buffered in time, and lets the user transform points, vectors, etc
 between any two coordinate frames at any desired point in time.
 .
 This package contains the Python 3 bindings and is part of Robot OS (ROS)

Package: python3-tf-conversions
Section: python
Architecture: any
Depends:
 python3-genpy,
 python3-pykdl,
 python3-std-msgs,
 python3-tf,
 ${misc:Depends},
 ${python3:Depends},
Description: Robot OS conversion library between Eigen, KDL and tf - Python 3 interface
 This package contains a set of conversion functions to convert
 common tf datatypes (point, vector, pose, etc) into semantically
 identical datatypes used by other libraries. The conversion functions
 make it easier for users of the transform library (tf) to work with
 the datatype of their choice. Currently this package has support for
 the Kinematics and Dynamics Library (KDL) and the Eigen matrix library.
 .
 This package contains the Python 3 bindings and is part of Robot OS (ROS).

Package: tf-tools
Section: utils
Architecture: any
Depends:
 graphviz,
 python3,
 python3-tf,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Set of utilities to operate with the Robot OS tf lib
 This package is part of Robot OS (ROS), and contains transform
 library tools. Although tf is mainly a code library meant to be used
 within ROS nodes, it comes with a large set of command-line tools
 that assist in the debugging and creation of tf coordinate
 frames. These tools include: static_transform_publisher,
 tf_change_notifier, tf_echo, tf_empty_listener tf_monitor, tf_remap
 and view_frames_tf
